<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

    Route::post('v1/generateToken', 'App\Http\Controllers\UserController@generateToken');
    Route::post('v1/register', 'App\Http\Controllers\UserController@register');
    Route::post('v1/login', 'App\Http\Controllers\UserController@authenticate');
    Route::group(['prefix' => 'v1','middleware' => ['jwt.verify']], function() {
        Route::get('/user', 'App\Http\Controllers\UserController@getAuthenticatedUser');
        Route::post('/create-loan', 'App\Http\Controllers\LoanController@createLoan');
        Route::post('/approve-loan', 'App\Http\Controllers\LoanController@approveLoan');
    });
