<?php
    namespace App\Http\Controllers;
    use App\Models\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Validator;
    use JWTAuth;
    use Tymon\JWTAuth\Exceptions\JWTException;
    use App\Models\{
    AffiliateKey
    };
    class UserController extends Controller
    {
        public function authenticate(Request $request)
        {
            $credentials = $request->only('email', 'password');

            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            return response()->json(compact('token'));
        }

        public function generateToken(Request $request){
             try{
            $api_key_requested = $request->header('api-key');
            $user_id = User::where('api_key',$api_key_requested)->value('id');
            if($user_id){
                $check = User::where('id',$user_id)->first();
                 if($check)
                    $check->api_key = $api_key_requested;
                    $token = JWTAuth::claims(['api_key' => $api_key_requested])->fromUser($check);
                    if($token)
                    {
                        return response()->json(array('status'=>true,'message'=>'Token generated successfully.','data'=>['token' => $token,'status_code'=>200]),200);
                    }
                    else{
                        return response()->json(array('status'=>false,'message'=>'Please Check your credentials. Token not created.','status_code'=>400),400);    
                    }
                }
                else{
                    return response()->json(array('status'=>false,'message'=>"Record not found.",'status_code'=>400),400);
                }
        }
        catch(\Exception $e){
            $response=['status' => false,'message'=>'Something went wrong. Please try again later.','status_code'=>400];
            $status=400;
            return response()->json($response,$status);
        }
        }

        public function register(Request $request)
        {
                $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);

            if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
            }

            $user = User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'api-key' => 'Z1130-U975W-VV0WV-19997-6830U',
                'password' => Hash::make($request->get('password')),
            ]);

            $token = JWTAuth::fromUser($user);

            return response()->json(compact('user','token'),201);
        }

        public function getAuthenticatedUser()
            {
                    try {

                            if (! $user = JWTAuth::parseToken()->authenticate()) {
                                    return response()->json(['user_not_found'], 404);
                            }

                    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                            return response()->json(['token_expired'], $e->getStatusCode());

                    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                            return response()->json(['token_invalid'], $e->getStatusCode());

                    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                            return response()->json(['token_absent'], $e->getStatusCode());

                    }

                    return response()->json(compact('user'));
            }
    }