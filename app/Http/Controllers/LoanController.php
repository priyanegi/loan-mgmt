<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\{
    Loan,User,ApprovedLoan
};
use Validator;
class LoanController extends Controller
{
    
    public function createLoan(Request $request){
            try{
                $validator = \Validator::make($request->all(), ['user_id' => 'required', 'amount' => 'required','duration'=>'required']);
                if ($validator->fails()) {
                   return response()->json($validator->errors(), 422);
                }
                $findUser = User::find($request->input('user_id'));
                if(isset($findUser)){
                    $findExistingLoan = Loan::where('user_id',$request->input('user_id'))->where('is_deleted','0')->get();
                    if(count($findExistingLoan) > 0){
                        return response()->json(array('success' => 'false', 'message' => 'Loan application already available for this user.','status_code'=>400),400);
                    }else{
                          $createLoan = new Loan();
                          $createLoan->user_id = $request->input('user_id');
                          $createLoan->amount = $request->input('amount');
                          $createLoan->duration = $request->input('duration');
                          if($request->input('is_active'))
                            $createLoan->is_active = $request->input('is_active');
                          if($request->input('term_condition'))
                            $createLoan->term_condition = $request->input('term_condition');
                          if($request->input('term_status'))
                            $createLoan->term_status = $request->input('term_status');
                          if($request->input('is_deleted'))
                            $createLoan->is_deleted = $request->input('is_deleted');
                          $createLoan->save();
                          return response()->json(array('success' => 'true', 'message' => 'Loan application created successfully.Loan application will activate on loan approval.','status_code'=>200),200);
                    }
                }else{
                    return response()->json(array('success' => 'false', 'message' => 'No record found.','status_code'=>404),404);
                }
             } catch(\Exception $err){
                return array('status' => false, 'message' => $err->getMessage(), 'status_code' => 400);
            } 
        }

    public function approveLoan(Request $request)
    {
        try{
            $rules =[];
            $msgs =[];
            if($request->input('loan_id')==""){
                    $rules['loan_id'] ='required';
                    $msgs['loan_id.required']='Loan id field is required.';
                }
                if(!empty($rules)){
                    $validator = Validator::make($request->all(),$rules,$msgs);  
                    if($validator->fails()){
                        return  response()->json(array('status' =>false,'errors' => $validator->errors(),'status_code'=>422),422);
                    }
                }
            $findLoan = Loan::where('id',$request->input('loan_id'))->where('is_active','0')->where('is_deleted','0')->get();
            if(count($findLoan) > 0){
                $approveLoanData = Loan::find($request->input('loan_id'));
                $approveLoanData->is_active = '1';
                $approveLoanData->update();
                $this->updateApprovedList($request->input('loan_id'));
                return response()->json(array('success' => 'true', 'message' => 'Loan approved successfully.','status_code'=>200),200);
            }else{
                $approvedLoan = Loan::where('id',$request->input('loan_id'))->where('is_active','1')->where('is_deleted','0')->get();
                if(count($approvedLoan) > 0){
                 return response()->json(array('success' => 'true', 'message' => 'Already approved.','status_code'=>200),200); 
                }else{
                   return response()->json(array('success' => 'false', 'message' => 'Loan not found.','status_code'=>404),404); 
                }
            }
        } catch(\Exception $err){
            return array('status' => false, 'message' => $err->getMessage(), 'status_code' => 400);
        } 
    }

    public function updateApprovedList($loanId){
        $createApprovedLoan = new ApprovedLoan();
        $createApprovedLoan->loan_id = $loanId;
        $createApprovedLoan->save();
        return;
    }
}
